/**
  ******************************************************************************
  * @Author         [M.MeHdi Naseri]
  * @StudentNumber  [9423115]
  * @ProjectName    Smart Home
  * @Date           22-January-2018
  * @brief          Main program body
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

/** @addtogroup Template_Project
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void initGPIO(void);
void delay(uint64_t);
float averageOfArray(float array[], float L);

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{	
	/* Variables */
	float temperature = 0;
	float light = 0;
	float moisture = 0;
	_Bool	isDay = 0;
	float T[10] = {0};
	float L[10] = {0};
	float M[10] = {0};
	uint16_t c = 0; //counter for arrays
	float temperatureAverage = 0;
	float lightAverage = 0;
	float moistureAverage = 0;

	/* GPIO init */
	initGPIO();

  /* Infinite loop */
  while(1)
  {
		/* Save T, M, L */
		temperature = ((float)((uint8_t)GPIOA->ODR))/2 - 38;
		light = ((uint8_t)(GPIOA->ODR >> 8))*32;
		moisture = ((float)((uint8_t)GPIOB->ODR))/2;
		isDay = (_Bool)((GPIOB->ODR) >> 8);
		
		/* Calculate average of 10 previous values */
		temperatureAverage = averageOfArray(T, 10);
		lightAverage = averageOfArray(L, 10);
		moistureAverage = averageOfArray(M, 10);
		
		/* Temperature */
		if(temperature > 15 && temperatureAverage < 15)
			GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFFC) | 0x00000001;
		
		else if(temperature > 30 && temperatureAverage < 30)
			GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFFC) | 0x00000002;
		
		else if(temperature > 45 && temperatureAverage < 45)
			GPIOC->IDR = GPIOC->IDR | 0x00000003;
		
		else if(temperature < 40 && temperatureAverage > 40)
			GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFFC) | 0x00000002;
		
		else if(temperature < 25 && temperatureAverage > 25)
			GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFFC) | 0x00000001;
		
		else if(temperature < 10 && temperatureAverage > 10)
			GPIOC->IDR = GPIOC->IDR & 0xFFFFFFFC;
		
		/* Light */
		if(!isDay) //isNight
		{
			if(light > 2000 && lightAverage < 2000)
				GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFF3) | 0x00000008;
			
			else if(light > 4000 && lightAverage < 4000)
				GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFF3) | 0x00000004;
			
			else if(light > 6000 && lightAverage < 6000)
				GPIOC->IDR = GPIOC->IDR & 0xFFFFFFF3;
			
			else if(light < 5000 && lightAverage > 5000)
				GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFF3) | 0x00000004;
			
			else if(light < 3000 && lightAverage > 3000)
				GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFF3) | 0x00000008;
			
			else if(light < 1000 && lightAverage > 1000)
				GPIOC->IDR = GPIOC->IDR | 0x0000000C;
		}
		else if(isDay)
		{
			if(light > 2000 && lightAverage < 2000)
				GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFEF) | 0x00000020;
			
			else if(light > 4000 && lightAverage < 4000)
				GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFDF) | 0x00000010;
			
			else if(light > 6000 && lightAverage < 6000)
				GPIOC->IDR = GPIOC->IDR & 0xFFFFFFCF;
			
			else if(light < 5000 && lightAverage > 5000)
				GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFDF) | 0x00000010;
			
			else if(light < 3000 && lightAverage > 3000)
				GPIOC->IDR = (GPIOC->IDR & 0xFFFFFFEF) | 0x00000020;
			
			else if(light < 1000 && lightAverage > 1000)
				GPIOC->IDR = GPIOC->IDR | 0x00000030;
		}
		
		/* Moisture */
		if(moisture > 25 && moistureAverage < 25)
			GPIOC->IDR = (GPIOC->IDR & 0xFFFFFF3F) | 0x00000080;
		
		else if(moisture > 50 && moistureAverage < 50)
			GPIOC->IDR = (GPIOC->IDR & 0xFFFFFF3F) | 0x00000040;
		
		else if(moisture > 75 && moistureAverage < 75)
			GPIOC->IDR = GPIOC->IDR & 0xFFFFFF3F;
		
		else if(moisture < 65 && moistureAverage > 65)
			GPIOC->IDR = (GPIOC->IDR & 0xFFFFFF3F) | 0x00000040;
		
		else if(moisture < 40 && moistureAverage > 40)
			GPIOC->IDR = (GPIOC->IDR & 0xFFFFFF3F) | 0x00000080;
		
		else if(moisture < 15 && moistureAverage > 15)
			GPIOC->IDR = GPIOC->IDR | 0x000000C0;
		
		/* go to first element if reach the end */
		if(c == 10)
			c = 0;
		
		/* Save new values in Arrays */
		T[c] = temperature;
		L[c] = light;
		M[c] = moisture;
		c++;
		
		/* Delay */
		delay(5);
  }	
}

void initGPIO(void)
{
	/* Clock */
	RCC->AHB1ENR |= 0x0000001F;     //Enable GPIOA, GPIOB, GPIOC, GPIOD and GPIOE clock in AHB1
	
	/* PORT A */
	GPIOA->MODER = 0x00000000;	
	GPIOA->OSPEEDR = 0x55555555;
	GPIOA->PUPDR = 0x00000000;

	/* PORT B */
	GPIOB->MODER = 0x00000000;
	GPIOB->OSPEEDR = 0xAAAAAAAA;
	GPIOB->PUPDR = 0x00000000;
	
	/* PORT C */
	GPIOC->MODER = 0x55555555;
	GPIOC->OTYPER &= 0xFFFF0000;
	GPIOC->OSPEEDR = 0xFFFFFFFF;
	GPIOC->PUPDR = 0x55555555;
	
	/* PORT D */
	GPIOD->MODER = 0x55555555;
	GPIOD->OTYPER |= 0x0000FFFF;
	GPIOD->OSPEEDR = 0x00000000;
	GPIOD->PUPDR = 0xAAAAAAAA;
	
	/* PORT E */
	GPIOE->MODER = 0xFFFFAAAA;
	GPIOE->OSPEEDR = 0xAAAAFFFF;
	GPIOE->PUPDR = 0x00000000;
}

void delay(uint64_t n)
{
	n *= 1000000; 
	while(n != 0){n -= 1;}
}

float averageOfArray(float array[], float L)
{
	float average = 0;
	int i = 0;
	for(i = 0; i < L; i++)
		average += array[i];
	average /= L;
	return average;
}
